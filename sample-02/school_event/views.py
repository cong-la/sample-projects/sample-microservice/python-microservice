import requests
from django.http import JsonResponse
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser

from school_event.models import SchoolEvent
from school_event.school_event_serializer import SchoolEventSerializer


@api_view(['GET'])
def get_all_events(request):
    school_event = SchoolEvent.objects.all()
    school_event_serializer = SchoolEventSerializer(school_event, many=True)
    response_data = school_event_serializer.data
    return JsonResponse({'response': response_data}, safe=False, status=status.HTTP_200_OK)


@api_view(['GET'])
def get_event_by_id(request, id):
    try:
        # GET EVENT INFO
        school_event = SchoolEvent.objects.get(id=id)
        school_event_serializer = SchoolEventSerializer(school_event, many=False)
        response_data = school_event_serializer.data

        # GET SCHOOL INFO
        school_response = requests.get('http://sample-01-web-1:8001/info/1').json()

        # Create a network
        # docker network create --network's name--
        # Put the containers both containerA and containerB in this network
        # docker network connect --network's name-- containerA
        # docker network connect --network's name-- containerB
        # You can access one container to another container with container name now.

        return JsonResponse({'response': response_data, 'school_info': school_response}, safe=False, status=status.HTTP_200_OK)
    except:
        return JsonResponse({'response': school_event_serializer.errors}, safe=False, status=status.HTTP_204_NO_CONTENT)


@api_view(['GET'])
def get_event_by_school_id(request, school_id):
    try:
        school_event = SchoolEvent.objects.all().filter(school_id=school_id)
        school_event_serializer = SchoolEventSerializer(school_event, many=True)
        response_data = school_event_serializer.data
        return JsonResponse({'response': response_data}, safe=False, status=status.HTTP_200_OK)
    except:
        return JsonResponse({'response': []}, safe=False, status=status.HTTP_204_NO_CONTENT)


@api_view(['POST'])
def add_new_event(request):
    event_data = JSONParser().parse(request)
    event_data_serializer = SchoolEventSerializer(data=event_data)
    if event_data_serializer.is_valid():
        event_data_serializer.save()
        return JsonResponse({'response': event_data_serializer.data}, safe=False, status=status.HTTP_201_CREATED)
    return JsonResponse({'response': 'Bad request.'}, safe=False, status=status.HTTP_400_BAD_REQUEST)
