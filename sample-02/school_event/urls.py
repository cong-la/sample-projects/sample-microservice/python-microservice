from django.contrib import admin
from django.urls import path

from school_event.views import *

urlpatterns = [
    path('', get_all_events),
    path('school/<int:school_id>', get_event_by_school_id),
    path('<int:id>', get_event_by_id),
    path('add/', add_new_event),
]