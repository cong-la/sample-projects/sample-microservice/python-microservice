from rest_framework.serializers import ModelSerializer

from school_info.models import SchoolInfo


class SchoolInfoSerializer(ModelSerializer):
    class Meta:
        model = SchoolInfo
        fields = ['id',
                  'name',
                  ]
